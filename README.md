This is a few simple plugins for [Xymon](http://sourceforge.net/projects/hobbitmon/) monitoring system.
All plugins licensed under GPL v.3


All plugins tested on Debian 8 / Ubuntu 16 instances.
Other platforms can require manual (without) make installation.

### Requirements

Installed Xymon, bash, git, make

### Installation

1. `git clone https://bitbucket.org/anmcarrow/xymonplugins.git /usr/local/xymonplugins`
2. `cd /usr/local/xymonplugins`
3. `make install`
4. add "mysql" check to the particular host description in /etc/xymon/hosts.cfg on your xymon-server instance

### Uninstallation

1. `cd /usr/local/xymonplugins`
2. `make uninstall`
3. remove "mysql" check from the particular host description in /etc/xymon/hosts.cfg on your xymon-server instance.

### Manual installation

1. `git clone https://bitbucket.org/anmcarrow/xymonplugins.git /usr/local/xymonplugins`
2. Add string "directory /usr/local/xymonplugins/etc" in your "$HOBBITCLIENTHOME/etc/tasks.cfg" (for Xymon-server machine or "$HOBBITCLIENTHOME/etc/clientlaunch.cfg" (for Xymon-client machine)
3. Or add strings "/usr/local/xymonplugins/etc/%PLUGINNAME%" to conffiles in individual order
4. Reload or restart Xymon service by any appropriate method

### List of content

- bin/mysqlcheck.sh — simple socket-based check for local MySQL service
- etc/mysqlcheck.cfg — confguration file for `bin/mysqlcheck.sh`

- bin/memcachedcheck.sh — similiar socket-based check for local Memcached service
- etc/memcachedcheck.cfg — confguration file for `bin/memcachedcheck.sh`

- bin/redischeck.sh — similiar socket-based check for local Redis service
- etc/redischeck.cfg — confguration file for `bin/redischeck.sh`
