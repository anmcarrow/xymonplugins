#!/bin/bash

   COLUMN=mysql
   COLOR=green
   MSG="MySQL is offline"
   PORT="3306"
   
   if $(netstat -nl | grep ${PORT} > /dev/null )
   then
      MSG="MySQL is online"
   else
      COLOR=red
      MSG="${MSG}"
   fi

   # Tell Xymon about it
  $XYMON $XYMSRV "status $MACHINE.$COLUMN $COLOR $(date) ${MSG}"

exit 0
