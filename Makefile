INCLUDE = "directory /usr/local/xymonplugins/etc/"
DELPATT = \/usr\/local\/xymonplugins
TG1 = "/etc/xymon/tasks.cfg"
TG2 = "/etc/xymon/clientlaunch.cfg"

.PHONY: help install uninstall

help:
	@echo " \
	make <install-server|install-client|uninstall-server|uninstall-client> \
	"

uninstall-server:
	@echo "Deleteting includes..."
	@sed -i "/$(DELPATT)/d" 2>/dev/null $(TG1) 
	@service xymon reload


uninstall-client:
	@echo "Deleteting includes..."
	@sed -i "/$(DELPATT)/d" 2>/dev/null $(TG2) 
	@service xymon-client restart

install-server: /etc/xymon/tasks.cfg
	@echo "Editing configurations..."
	@echo "$(INCLUDE)" >> $(TG1)
	@service xymon restart

install-client: /etc/xymon/clientlaunch.cfg
	@echo "Editing configurations..."
	@echo "$(INCLUDE)" >> $(TG2)
	@service xymon-client restart
